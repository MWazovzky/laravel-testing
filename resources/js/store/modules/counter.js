const state = {
    counter: 0,
}

const getters =  {
    getCounter: state => state.counter,
    getNextCounter: state => state.counter + 1,
}

const mutations = {
    setCounter (state, { counter }) {
        state.counter = counter
    }
}

const actions = {
    log (context, value) {
        return new Promise((resolve, reject) => {
            if (!value) reject(new Error('No value provided'))

            setTimeout(() => {
                console.log(`New counter value is ${value}`)
                resolve()
            }, 200)
        })
    },

    increment ({ dispatch, commit, state }) {
        return new Promise((resolve, reject) => {
            const oldValue = state.counter
            const newValue = oldValue + 1

            commit('setCounter', { counter: newValue })
            dispatch('log', newValue)
            resolve(newValue)
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}

export {
    state,
    getters,
    actions,
    mutations
}
