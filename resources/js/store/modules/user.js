import { user as api } from '../../api'

const state = {
    user: null,
}

const getters =  {
    getUser: state => state.user,
}

const mutations = {
    setUser (state, value) {
        state.user = value
    }
}

const actions = {
    loadUser ({ commit }) {
        return api.get()
            .then(({ data }) => commit('setUser', data))
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}

export {
    state,
    getters,
    actions,
    mutations
}
