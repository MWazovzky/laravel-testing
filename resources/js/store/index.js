import Vue from 'vue'
import Vuex from 'vuex'
import counter from './modules/counter'
import user from './modules/user'
import { getters as counterGetters, mutations as counterMutations, actions as counterActions } from './modules/counter'
import { getters as userGetters, mutations as userMutations, actions as userActions } from './modules/user'


Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        counter,
        user,
    }
})

const options = {
    getters: { 
        ...counterGetters,
        ...userGetters,
    },
    mutations: { 
        ...counterMutations ,
        ...userMutations,
    },
    actions: { 
        ...counterActions,
        ...userActions,
    },
}

export {
    options
}
