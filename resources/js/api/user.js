/**
 * Get user.
 *
 * @return { Promise }
 */

export const get = () => {
    return axios.get(`/api/user`)
}