<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_has_name()
    {
        $user = factory('App\User')->create(['name' => 'John']);

        $this->assertEquals('John', $user->name);
    }
}
