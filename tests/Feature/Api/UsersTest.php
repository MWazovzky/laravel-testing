<?php

namespace Tests\Feature\Api;

use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_get_user()
    {
        $this->withoutExceptionHandling();
        
        $user = factory('App\User')->create(['name' => 'John']);
        Passport::actingAs($user);

        $response = $this->get(route('api.users.get', $user));
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'name' => 'John',
            ]);
    }
}