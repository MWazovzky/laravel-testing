<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_get_user()
    {
        $this->withoutExceptionHandling();
        
        $user = factory('App\User')->create(['name' => 'John']);
        $this->actingAs($user);

        $this->get(route('users.show', $user))
            ->assertStatus(200)
            ->assertViewHas('name', $user->name);
    }
}
