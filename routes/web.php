<?php

use App\User;

Route::middleware('auth')->group(function() {
    Route::get('/users/{user}', function (User $user) {
        return view('users.show', ['name' => $user->name]);
    })->name('users.show');
});

Route::view('/test', 'test');
Route::view('/e2e', 'e2e');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
